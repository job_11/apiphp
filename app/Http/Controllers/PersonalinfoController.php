<?php

namespace App\Http\Controllers;

use App\Models\personalinfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PersonalinfoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $count = DB::table('personalinfos')->paginate(5);
        return response([
            'message' => 'success',
            'data' => $count,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $request->validate([
            'firstName' => 'required|string|max:20',
            'lastName' => 'required|string|max:20',
            'gender' => 'string|max:20',
            'dateOfBirth' => 'required|date',
            'age' => 'required|int|digits_between:1,4',
        ]);

        $pInfo = personalinfo::create([
            'Firstname' => $request->firstName,
            'Middlename' => $request->middleName,
            'Lastname' => $request->lastName,
            'Gender' => $request->gender,
            'DateOfBirth' => $request->dateOfBirth,
            'Age' => $request->age,
        ]);

        return response([
            'message' => 'success',
            'data' => $pInfo,
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show($srch = null)
    {
        $sInfo = DB::table('personalinfos')->where('Firstname', 'like', '%' . $srch . '%')
            ->orWhere('Middlename', 'like', '%' . $srch . '%')
            ->orWhere('Lastname', 'like', '%' . $srch . '%')
            ->orWhere('id',   $srch)
            ->get();

        if (!$srch) {
            $count = DB::table('personalinfos')
                ->orderByRaw('id DESC')
                ->get();
            return response([
                'message' => 'success',
                'data' => $count,
            ]);
        }
        if (count($sInfo) === 0) {
            return response([
                'message' => 'Not found',
                'data' => [],
            ], 200);
        }

        return response([
            'message' => 'success ',
            'data' => $sInfo,
        ]);
    }
    /**
     * Update the specified resource in storage.
     */
    // public function update($id = null)
    public function update($id = null, Request $request)
    {

        // $request = Request();

        $request->validate([
            'firstName' => 'required|string|max:20',
            'lastName' => 'required|string|max:20',
            'gender' => 'string|max:20',
            'dateOfBirth' => 'required|date',
            'age' => 'required|int|digits_between:1,4',
        ]);

        if (!$id) {
            return response([
                "message" => "error",
                "errors" =>  [
                    "id" => [
                        "The id field must be a int."
                    ]
                ]
            ]);
        }

        $uptInfo = personalinfo::where('id', $id)->first();

        if (is_null($uptInfo)) {
            return response([
                "message" => "Not found",
                "data" =>  []
            ]);
        }

        $uptInfo->Firstname = $request->firstName;
        $uptInfo->Middlename = $request->middleName;
        $uptInfo->Lastname = $request->lastName;
        $uptInfo->Gender = $request->gender;
        $uptInfo->DateOfBirth = $request->dateOfBirth;
        $uptInfo->Age = $request->age;

        $uptInfo->save();

        return response([
            'message' => 'success',
            'data' => $uptInfo,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id = null)
    {

        if (!$id) {
            return response([
                "message" => "error",
                "errors" =>  [
                    "id" => [
                        "The id field must be a int."
                    ]
                ]
            ]);
        }

        $del = personalinfo::where('id', $id)->delete();
        if ($del == 0) {
            return response([
                'message' => 'Not found',
                'data' => '',
            ], 404);
        }
        return response([
            'message' => 'success',
            'data' => '',
        ]);
    }
}

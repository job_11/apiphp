<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class personalinfo extends Model
{
    use HasFactory;

    protected $fillable = ['Firstname', 'Middlename', 'Lastname', 'Gender', 'DateOfBirth', 'Age'];


    protected $hidden = ['updated_at', 'created_at'];
}

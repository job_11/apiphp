<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('personalinfos', function (Blueprint $table) {
            $table->id();
            $table->string('Firstname', 20);
            $table->string('Middlename', 20)->nullable();
            $table->string('Lastname', 20);
            $table->string('Gender', 20);
            $table->date('DateOfBirth');
            $table->integer('Age')->length(10)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('personalinfos');
    }
};

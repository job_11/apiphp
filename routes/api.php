<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PersonalinfoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::controller(PersonalinfoController::class)->group(function () {
    Route::post('/register', 'store');
    Route::put('/update/{id}', 'update');
    Route::delete('/delete/{id?}', 'destroy');
    Route::get('/pinfo/{srch?}', 'show');
    Route::get('/', 'index');
});

// Route::middleware(['auth:sanctum'])->get('/user', function (Request $request) {
//     return $request->user();
// });
